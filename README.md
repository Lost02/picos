# PicOS
### An os for the Rpi pico.
## How to Access?

1. Open your desired terminal app on a Linux system.
2. Run the "main" file on that system.
3. Connect the Rpi Pico to that (The "main.py" file on the pico will automatically run)
4. If the "main.py" file on your linux system detects the Rpi Pico you will be ready to start!

# Dependencies
